/* global define */

define([
  'jquery',
  'jquery.jstree',
  'jquery.jsPlumb'
], function ($, jstree, jsPlumb) {
  'use strict';
	
	window.jsPlumbDemo = {
			
		init : function(parentChildMap) {
			
			jsPlumb.importDefaults({
				Connector:"StateMachine",
				PaintStyle:{ lineWidth:2, strokeStyle:"#464646" },
				Endpoint:[ "Dot", { radius:3 } ],
				EndpointStyle:{ fillStyle:"#464646" }
			});
			  
			var shapes = $(".shape");
				
			// make everything draggable
			jsPlumb.draggable(shapes);
			
			for (var i in parentChildMap) {      
				console.log("Processing " + i + ":");
				if (i === "root") {
					continue;
				}
				if ((i != null) && (parentChildMap[i] != null)) {

					srcShape = $('#' + i);
					if (srcShape != null) {
						console.log("Found source shape.");
					}

					for (var elem in parentChildMap[i]) {
						console.log("Processing " + parentChildMap[i][elem] + ":");
					tgtShape = $('#' + parentChildMap[i][elem]);
					if (tgtShape != null) {
						console.log("Found target shape.");
					}

				if (srcShape != null && tgtShape != null) {

				jsPlumb.connect({
						source:srcShape,  // just pass in the current node in the selector for source 
						target:tgtShape,
						// here we supply a different anchor for source and for target, and we get the element's "data-shape"
						// attribute to tell us what shape we should use, as well as, optionally, a rotation value.
						anchors:[
							[ "Perimeter", { shape:"Rectangle" }],
							[ "Perimeter", { shape:"Rectangle" }]
						]
					});				
					}
					}
				}
			}
			/*
			// loop through them and connect each one to each other one.
			for (var i = 0; i < shapes.length; i++) {
				for (var j = i + 1; j < shapes.length; j++) {						
					jsPlumb.connect({
						source:shapes[i],  // just pass in the current node in the selector for source 
						target:shapes[j],
						// here we supply a different anchor for source and for target, and we get the element's "data-shape"
						// attribute to tell us what shape we should use, as well as, optionally, a rotation value.
						anchors:[
							[ "Perimeter", { shape:$(shapes[i]).attr("data-shape"), rotation:$(shapes[i]).attr("data-rotation") }],
							[ "Perimeter", { shape:$(shapes[j]).attr( "data-shape"), rotation:$(shapes[j]).attr("data-rotation") }]
						]
					});				
				}	
			}*/
    }    
  }
});