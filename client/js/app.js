/*
 * client/js/app.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'backbone.marionette',
  'router',
  'wreqr',
  'views/main',
  'views/dashboard/dashboard',
  'views/dashboard/manage',
  'views/dashboard/objects',
  'views/dashboard/transform',
  'views/dashboard/serve',
  'views/dashboard/test',
  'views/dashboard/billing_overview',
  'views/dashboard/billing_invoice',
  'views/dashboard/support'
], function ($, Backbone, Marionette, Router, wreqr, MainView, DashboardView, ManageView, ObjectsView, TransformView, ServeView, TestView, BillingOverviewView, BillingInvoiceView, SupportView) {
  'use strict';

  var app = new Marionette.Application();

  app.addRegions({
    mainRegion: '#main'
  });

  app.addInitializer(function (screen) {
    if(screen === 'main'){
      app.mainRegion.show(new MainView());
    } else if(screen === 'dashboard'){
      app.mainRegion.show(new DashboardView());
    } else if(screen === 'manage'){
      app.mainRegion.show(new ManageView());
    } else if(screen === 'objects'){
      app.mainRegion.show(new ObjectsView());
    } else if(screen === 'transform'){
      app.mainRegion.show(new TransformView());
    } else if(screen === 'serve'){
      app.mainRegion.show(new ServeView());
    } else if(screen === 'test'){
      app.mainRegion.show(new TestView());
    } else if(screen === 'billing_overview'){
      app.mainRegion.show(new BillingOverviewView());
    } else if(screen === 'billing_invoice'){
      app.mainRegion.show(new BillingInvoiceView());
    } else if(screen === 'support'){
      app.mainRegion.show(new SupportView());
    } else if(screen === 'about'){
      app.mainRegion.show(new MainView());
    } else if(screen === 'advertise'){
      app.mainRegion.show(new MainView());
    } else if(screen === 'blog'){
      app.mainRegion.show(new MainView());
    } else if(screen === 'careers'){
      app.mainRegion.show(new MainView());
    } else if(screen === 'contact'){
      app.mainRegion.show(new MainView());
    } else if(screen === 'help'){
      app.mainRegion.show(new MainView());
    }
  });

  app.on('initialize:after', function () {
    // Initialize router
    new Router();

    // Begin monitoring hash changes.
    Backbone.history.start({ pushState: true });

    // Snatch all click events on anchor tags.
    // $(document).on('click', 'a:not([target])', function (event) {
    //   var href = $(this).attr('href');
    //   var protocol = this.protocol + '//';

    //   if (href.slice(protocol.length) !== protocol) {
    //     event.preventDefault();
    //     Backbone.history.navigate(href, { trigger: true });
    //   }
    // });
  });

  wreqr.vent.on('vent:test', function () {
    console.log('test:vent event received.');
  });

  wreqr.vent.on('socketio:received', function (data) {
    console.log('socketio:received - ' + JSON.stringify(data));
  });

  wreqr.vent.trigger('vent:test');

  return app;
});
