/*
 * client/js/collections/manage.js
 */

/* global define */

define([
  'backbone',
  'models/api'
], function (Backbone, API) {
  'use strict';

  return Backbone.Collection.extend({
    model: API,
    url: '/api/api'
  });
});
