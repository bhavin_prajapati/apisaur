/*
 * client/js/models/api.js
 */

/* global define */

define([
  'backbone'
], function (Backbone) {
  'use strict';

  return Backbone.Model.extend({

  });

});
