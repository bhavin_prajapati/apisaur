/*
 * client/js/views/dashboard/billing_invoice.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'hbs!templates/dashboard/billing_invoice'
], function ($, Backbone, billingInvoiceTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: billingInvoiceTpl
  });

});
