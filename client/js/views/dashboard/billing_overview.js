/*
 * client/js/views/dashboard/billing_overview.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'hbs!templates/dashboard/billing_overview'
], function ($, Backbone, billingOverviewTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: billingOverviewTpl
  });

});
