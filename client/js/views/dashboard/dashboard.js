/*
 * client/js/views/dashboard/dashboard.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'hbs!templates/dashboard/dashboard'
], function ($, Backbone, dashboardTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: dashboardTpl
  });

});
