/*
 * client/js/views/dashboard/manage.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'handlebars',
  'ace',
  'collections/manage',
  'hbs!templates/home/layout',
  'hbs!templates/dashboard/manage',
  'hbs!templates/dashboard/api'
], function ($, Backbone, Handlebars, ace, Content, layoutTpl, manageTpl, apiTpl) {
  'use strict';

  var LoadingView = Backbone.Marionette.ItemView.extend({
    template: Handlebars.compile(
      '<div style="height: 50px;">' +
      '<div style="margin-left: auto;margin-right: auto;width: 25px;">' +
      '<i class="icon-spinner icon-spin icon-2x"></i>' +
      '</div>' +
      '</div>'
    )
  });

  var EmptyView = Backbone.Marionette.ItemView.extend({
    template: Handlebars.compile('<em>N/A</em>')
  });

  var APIView = Backbone.Marionette.ItemView.extend({
    template: apiTpl,
    tagName: 'tr'
  });

  var APIListView = Backbone.Marionette.CompositeView.extend({
    template: manageTpl,
    itemViewContainer: 'tbody.apis',
    itemView: APIView,
    emptyView: EmptyView,

    initialize: function() {
      this.listenTo(this.collection, 'request', this.onRequest);
      this.listenTo(this.collection, 'sync', this.onSync);

      this.collection.fetch();
    },

    onRequest: function () {
      this.emptyView = LoadingView;
      this.render();
    },

    onSync: function () {
      this.emptyView = EmptyView;
      this.render();
    }
  });

  return Backbone.Marionette.Layout.extend({
    template: layoutTpl,

    regions: {
      mainRegion: '#layout'
    },

    onShow: function () {
      var content = new Content();
      this.mainRegion.show(new APIListView({
        collection: content
      }));
    }
  });
});
