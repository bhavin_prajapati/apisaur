/*
 * client/js/views/dashboard/objects.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'hbs!templates/dashboard/objects'
], function ($, Backbone, objectsTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: objectsTpl
  });

});
