/*
 * client/js/views/dashboard/serve.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'hbs!templates/dashboard/serve'
], function ($, Backbone, serveTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: serveTpl
  });

});
