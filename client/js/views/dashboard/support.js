/*
 * client/js/views/dashboard/support.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'hbs!templates/dashboard/support'
], function ($, Backbone, supportTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: supportTpl
  });

});
