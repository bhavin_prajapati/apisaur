/*
 * client/js/views/dashboard/test.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'hbs!templates/dashboard/test'
], function ($, Backbone, testTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: testTpl
  });

});
