/*
 * client/js/views/dashboard/transform.js
 */

/* global define */

define([
  'jquery',
  'jquery.ui',
  'jquery.ui.touch-punch',
  'jquery.dd',
  'jquery.jstree',
  'jquery.jsplumb',
  'jquery.splitter',
  'backbone',
  'hbs!templates/dashboard/transform'
], function ($, ui, uitouchpunch, dd, jstree, jsplumb, splitter, Backbone, transformTpl) {
  'use strict';

  // TODO: Decision needs to be made whether we support array object elements during transform
  var enableArrayObjectElements = false;

  /*
    HTML tag formatting:
    <apiName>-<fieldName>-<fieldKey>-<instanceKey>

    apiDirectory Object:
    Object -> [apiName, apiData Object]

    apiData object:
    Object -> [fieldKey, {<field information>}]

    Transformation storage:
    Object -> [fieldKey-instanceKey, {<transformation information>}]
  */

  // API Directory object
  var apiDirectory = {};
  // Transform list array
  var transformList = [];

  // User api selection
  var selectedApiName = null;
  // User api call selection
  var selectedApiCallName = null;



  var sampleTmpl = {
    path: '.',
    aggregate: {
      total: function (key, value, existing) {
        if (!isArray(value)) {
          return value;
        } else {
          return value.sort().reverse()[0];
        }
      },
      pages: function (key, value, existing) {
        if (!isArray(value)) {
          return value;
        } else {
          return value.sort().reverse()[0];
        }
      }
    },
    as: {
      bins: {
        path: 'Items.SearchBinSets.SearchBinSet.Bin',
        key: 'BinParameter.Value',
        value: 'BinItemCount',
        aggregate: function (key, value, existing) {
          return Math.max(value, existing || 0);
        }
      },
      items: {
        path: 'Items.Item',
        all: true,
        as: {
          rank: 'SalesRank',
          title: 'ItemAttributes.Title',
          artist: 'ItemAttributes.Artist',
          manufacturer: 'ItemAttributes.Manufacturer',
          category: 'ItemAttributes.ProductGroup',
          price: 'Offers.Offer.OfferListing.Price.FormattedPrice',
          percent_saved: 'Offers.Offer.OfferListing.PercentageSaved',
          availability: 'Offers.Offer.OfferListing.Availability',
          price_new: 'OfferSummary.LowestNewPrice.FormattedPrice',
          price_used: 'OfferSummary.LowestUsedPrice.FormattedPrice',
          url: 'DetailPageURL',
          similar: {
            path: 'SimilarProducts.SimilarProduct',
            key: 'ASIN',
            value: 'Title'
          },
          images: {
            path: '.',
            choose: ['SmallImage', 'MediumImage', 'LargeImage'],
            format: function (node, value, key) {
              return {
                key: key.replace(/Image$/, '').toLowerCase()
              };
            },
            nested: true,
            as: {
              url: 'URL',
              height: 'Height.#',
              width: 'Width.#'
            }
          },
          image_sets: {
            path: 'ImageSets.ImageSet',
            key: '@.Category',
            choose: function (node, value, key) {
              return key !== '@';
            },
            format: function (node, value, key) {
              return {
                key: key.replace(/Image$/, '').toLowerCase()
              };
            },
            nested: true,
            as: {
              url: 'URL',
              height: 'Height.#',
              width: 'Width.#'
            }
          },
          links: {
            path: 'ItemLinks.ItemLink',
            key: 'Description',
            value: 'URL'
          }
        }
      }
    }
  };

  var sampleData = {
    "@": {
      "xmlns": "http://"
    },
    "Items": {
      "Request": {
        "IsValid": "True",
        "ItemSearchRequest": {
          "ItemPage": "1",
          "Keywords": "steve jobs",
          "ResponseGroup": ["Offers", "Similarities", "Small", "SalesRank", "SearchBins", "Images"],
          "SearchIndex": "All"
        }
      },
      "TotalResults": "2143",
      "TotalPages": "215",
      "Item": [{
        "ASIN": "1451648537",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "4",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "49",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "105",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "329",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "20",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "49",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "49",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "72",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "105",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "329",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "Walter Isaacson",
          "Manufacturer": "Simon & Schuster",
          "ProductGroup": "Book",
          "Title": "Steve Jobs"
        },
        "OfferSummary": {
          "LowestNewPrice": {
            "Amount": "1344",
            "CurrencyCode": "USD",
            "FormattedPrice": "$13.44"
          },
          "LowestUsedPrice": {
            "Amount": "1199",
            "CurrencyCode": "USD",
            "FormattedPrice": "$11.99"
          },
          "LowestCollectiblePrice": {
            "Amount": "3795",
            "CurrencyCode": "USD",
            "FormattedPrice": "$37.95"
          },
          "TotalNew": "113",
          "TotalUsed": "50",
          "TotalCollectible": "12",
          "TotalRefurbished": "0"
        },
        "Offers": {
          "TotalOffers": "1",
          "TotalOfferPages": "1",
          "Offer": {
            "Merchant": {
              "MerchantId": "Deprecated"
            },
            "OfferAttributes": {
              "Condition": "New"
            },
            "OfferListing": {
              "OfferListingId": "ABC123",
              "Price": {
                "Amount": "1749",
                "CurrencyCode": "USD",
                "FormattedPrice": "$17.49"
              },
              "AmountSaved": {
                "Amount": "1751",
                "CurrencyCode": "USD",
                "FormattedPrice": "$17.51"
              },
              "PercentageSaved": "50",
              "Availability": "Usually ships in 24 hours",
              "AvailabilityAttributes": {
                "AvailabilityType": "now",
                "MinimumHours": "0",
                "MaximumHours": "0"
              },
              "IsEligibleForSuperSaverShipping": "1"
            }
          }
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "1932841660",
            "Title": "I, Steve: Steve Jobs in His Own Words"
          }, {
            "ASIN": "0743264746",
            "Title": "Einstein: His Life and Universe"
          }, {
            "ASIN": "0071636080",
            "Title": "The Presentation Secrets of Steve Jobs: How to Be Insanely Great in Front of Any Audience"
          }, {
            "ASIN": "0374275637",
            "Title": "Thinking, Fast and Slow"
          }, {
            "ASIN": "1451627289",
            "Title": "11/22/63: A Novel"
          }]
        }
      }, {
        "ASIN": "1932841660",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "1994",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "48",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "103",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "321",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "19",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "48",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "48",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "71",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "103",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "321",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Creator": {
            "#": "George Beahm",
            "@": {
              "Role": "Editor"
            }
          },
          "Manufacturer": "Agate B2",
          "ProductGroup": "Book",
          "Title": "I, Steve: Steve Jobs in His Own Words"
        },
        "OfferSummary": {
          "LowestNewPrice": {
            "Amount": "567",
            "CurrencyCode": "USD",
            "FormattedPrice": "$5.67"
          },
          "LowestUsedPrice": {
            "Amount": "595",
            "CurrencyCode": "USD",
            "FormattedPrice": "$5.95"
          },
          "TotalNew": "30",
          "TotalUsed": "11",
          "TotalCollectible": "0",
          "TotalRefurbished": "0"
        },
        "Offers": {
          "TotalOffers": "1",
          "TotalOfferPages": "1",
          "Offer": {
            "Merchant": {
              "MerchantId": "Deprecated"
            },
            "OfferAttributes": {
              "Condition": "New"
            },
            "OfferListing": {
              "OfferListingId": "ABC123",
              "Price": {
                "Amount": "743",
                "CurrencyCode": "USD",
                "FormattedPrice": "$7.43"
              },
              "AmountSaved": {
                "Amount": "352",
                "CurrencyCode": "USD",
                "FormattedPrice": "$3.52"
              },
              "PercentageSaved": "32",
              "Availability": "Usually ships in 24 hours",
              "AvailabilityAttributes": {
                "AvailabilityType": "now",
                "MinimumHours": "0",
                "MaximumHours": "0"
              },
              "IsEligibleForSuperSaverShipping": "1"
            }
          }
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "1451648537",
            "Title": "Steve Jobs"
          }, {
            "ASIN": "0071636080",
            "Title": "The Presentation Secrets of Steve Jobs: How to Be Insanely Great in Front of Any Audience"
          }, {
            "ASIN": "161893001X",
            "Title": "Fortune the Legacy of Steve Jobs 1955-2011: A Tribute from the Pages of Fortune Magazine"
          }, {
            "ASIN": "1591842972",
            "Title": "Inside Steve's Brain, Expanded Edition"
          }, {
            "ASIN": "1618930028",
            "Title": "Time Steve Jobs: The Genius Who Changed Our World"
          }]
        }
      }, {
        "ASIN": "0071636080",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "1984",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "44",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "94",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "295",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "18",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "44",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "44",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "65",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "94",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "295",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "Carmine Gallo",
          "Manufacturer": "McGraw-Hill",
          "ProductGroup": "Book",
          "Title": "The Presentation Secrets of Steve Jobs: How to Be Insanely Great in Front of Any Audience"
        },
        "OfferSummary": {
          "LowestNewPrice": {
            "Amount": "974",
            "CurrencyCode": "USD",
            "FormattedPrice": "$9.74"
          },
          "LowestUsedPrice": {
            "Amount": "879",
            "CurrencyCode": "USD",
            "FormattedPrice": "$8.79"
          },
          "TotalNew": "53",
          "TotalUsed": "37",
          "TotalCollectible": "0",
          "TotalRefurbished": "0"
        },
        "Offers": {
          "TotalOffers": "1",
          "TotalOfferPages": "1",
          "Offer": {
            "Merchant": {
              "MerchantId": "Deprecated"
            },
            "OfferAttributes": {
              "Condition": "New"
            },
            "OfferListing": {
              "OfferListingId": "ABC123",
              "Price": {
                "Amount": "1493",
                "CurrencyCode": "USD",
                "FormattedPrice": "$14.93"
              },
              "AmountSaved": {
                "Amount": "702",
                "CurrencyCode": "USD",
                "FormattedPrice": "$7.02"
              },
              "PercentageSaved": "32",
              "Availability": "Usually ships in 24 hours",
              "AvailabilityAttributes": {
                "AvailabilityType": "now",
                "MinimumHours": "0",
                "MaximumHours": "0"
              },
              "IsEligibleForSuperSaverShipping": "1"
            }
          }
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "007174875X",
            "Title": "The Innovation Secrets of Steve Jobs: Insanely Different Principles for Breakthrough Success"
          }, {
            "ASIN": "1451648537",
            "Title": "Steve Jobs"
          }, {
            "ASIN": "1932841660",
            "Title": "I, Steve: Steve Jobs in His Own Words"
          }, {
            "ASIN": "0321525655",
            "Title": "Presentation Zen: Simple Ideas on Presentation Design and Delivery"
          }, {
            "ASIN": "1593156391",
            "Title": "The Steve Jobs Way: iLeadership for a New Generation"
          }]
        }
      }, {
        "ASIN": "B004W2UBYW",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "24",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "49",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "105",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "328",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "20",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "49",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "49",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "72",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "105",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "328",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "Walter Isaacson",
          "Manufacturer": "Simon & Schuster",
          "ProductGroup": "eBooks",
          "Title": "Steve Jobs"
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "B005UQLLCU",
            "Title": "I, Steve: Steve Jobs in His Own Words"
          }, {
            "ASIN": "B004LROUNG",
            "Title": "The Litigators"
          }, {
            "ASIN": "B00555X8OA",
            "Title": "Thinking, Fast and Slow"
          }, {
            "ASIN": "B004J4X9L0",
            "Title": "Catherine the Great: Portrait of a Woman"
          }, {
            "ASIN": "B005CRQ2OE",
            "Title": "Boomerang: Travels in the New Third World"
          }]
        }
      }, {
        "ASIN": "030795028X",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "4556",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "49",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "104",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "326",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "20",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "49",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "49",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "72",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "104",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "326",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "Walter Isaacson",
          "Manufacturer": "Vintage",
          "ProductGroup": "Book",
          "Title": "Steve Jobs (Espanol): Edicion en Espanol (Vintage Espanol) (Spanish Edition)"
        },
        "OfferSummary": {
          "LowestNewPrice": {
            "Amount": "1075",
            "CurrencyCode": "USD",
            "FormattedPrice": "$10.75"
          },
          "LowestUsedPrice": {
            "Amount": "999",
            "CurrencyCode": "USD",
            "FormattedPrice": "$9.99"
          },
          "TotalNew": "26",
          "TotalUsed": "12",
          "TotalCollectible": "0",
          "TotalRefurbished": "0"
        },
        "Offers": {
          "TotalOffers": "1",
          "TotalOfferPages": "1",
          "Offer": {
            "Merchant": {
              "MerchantId": "Deprecated"
            },
            "OfferAttributes": {
              "Condition": "New"
            },
            "OfferListing": {
              "OfferListingId": "ABC123",
              "Price": {
                "Amount": "1221",
                "CurrencyCode": "USD",
                "FormattedPrice": "$12.21"
              },
              "AmountSaved": {
                "Amount": "574",
                "CurrencyCode": "USD",
                "FormattedPrice": "$5.74"
              },
              "PercentageSaved": "32",
              "Availability": "Usually ships in 24 hours",
              "AvailabilityAttributes": {
                "AvailabilityType": "now",
                "MinimumHours": "0",
                "MaximumHours": "0"
              },
              "IsEligibleForSuperSaverShipping": "1"
            }
          }
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "8483566354",
            "Title": "Las cuatro vidas de Steve Jobs (1955-2011) (Viva) (Spanish Edition)"
          }, {
            "ASIN": "1616058986",
            "Title": "Jacqueline Kennedy: Conversaciones historicas sobre mi vida con John F. Kennedy / Historic Conversations on Life With John F. Kennedy (Spanish Edition)"
          }, {
            "ASIN": "0307743519",
            "Title": "¡Basta de historias!: La obsesión latinoamericana con el pasado y las 12 claves del futuro (Vintage Espanol) (Spanish Edition)"
          }, {
            "ASIN": "0307744582",
            "Title": "Aleph (Espanol) (Vintage Espanol) (Spanish Edition)"
          }, {
            "ASIN": "6071109876",
            "Title": "El camino de Steve Jobs (The Steve Job's way: iLeadership for a New Generation) (Spanish Edition)"
          }]
        }
      }, {
        "ASIN": "B005CRQ29E",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "19430",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "56",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "120",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "375",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "22",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "56",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "56",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "82",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "120",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "375",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "The Editors of Fortune",
          "Manufacturer": "Fortune",
          "ProductGroup": "eBooks",
          "Title": "All About Steve: The Story of Steve Jobs and Apple from the Pages of Fortune"
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "B005UFUOGU",
            "Title": "WIRED: Steve Jobs, Revolutionary"
          }, {
            "ASIN": "B004GNFO7Q",
            "Title": "Apple's Cores 1: Steve Jobs and the Power of Passion"
          }, {
            "ASIN": "B004477DIW",
            "Title": "The Innovation Secrets of Steve Jobs : Insanely Different Principles for Breakthrough Success"
          }, {
            "ASIN": "B005UQLLCU",
            "Title": "I, Steve: Steve Jobs in His Own Words"
          }, {
            "ASIN": "B004W2UBYW",
            "Title": "Steve Jobs"
          }]
        }
      }, {
        "ASIN": "B005TPQ3C0",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "8841",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "56",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "119",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "373",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "22",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "56",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "56",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "82",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "119",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "373",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "Michael Essany",
          "Manufacturer": "Hyperink Books and eBooks",
          "ProductGroup": "eBooks",
          "Title": "The Life & Death Of Steve Jobs: \"One More Thing...\" (A Steve Jobs Biography & Hyperink Original)"
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "B005UFUOGU",
            "Title": "WIRED: Steve Jobs, Revolutionary"
          }, {
            "ASIN": "B004GNFO7Q",
            "Title": "Apple's Cores 1: Steve Jobs and the Power of Passion"
          }, {
            "ASIN": "B005UQLLCU",
            "Title": "I, Steve: Steve Jobs in His Own Words"
          }, {
            "ASIN": "B005CRQ29E",
            "Title": "All About Steve: The Story of Steve Jobs and Apple from the Pages of Fortune"
          }, {
            "ASIN": "B006B16JLQ",
            "Title": "Letters to Steve: Inside the E-mail Inbox of Apple's Steve Jobs"
          }]
        }
      }, {
        "ASIN": "B005UFUOGU",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "11839",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "56",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "120",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "375",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "22",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "56",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "56",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "82",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "120",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "375",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "Steven Levy",
          "Creator": {
            "#": "Chris Anderson",
            "@": {
              "Role": "Editor"
            }
          },
          "Manufacturer": "WIRED Magazine",
          "ProductGroup": "eBooks",
          "Title": "WIRED: Steve Jobs, Revolutionary"
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "B004GNFO7Q",
            "Title": "Apple's Cores 1: Steve Jobs and the Power of Passion"
          }, {
            "ASIN": "B005UQLLCU",
            "Title": "I, Steve: Steve Jobs in His Own Words"
          }, {
            "ASIN": "B005CRQ29E",
            "Title": "All About Steve: The Story of Steve Jobs and Apple from the Pages of Fortune"
          }, {
            "ASIN": "B006B16JLQ",
            "Title": "Letters to Steve: Inside the E-mail Inbox of Apple's Steve Jobs"
          }, {
            "ASIN": "B005TPQ3C0",
            "Title": "The Life & Death Of Steve Jobs: \"One More Thing...\" (A Steve Jobs Biography & Hyperink Original)"
          }]
        }
      }, {
        "ASIN": "007174875X",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "16567",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "49",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "104",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "325",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "20",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "49",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "49",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "72",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "104",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "325",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "Carmine Gallo",
          "Manufacturer": "McGraw-Hill",
          "ProductGroup": "Book",
          "Title": "The Innovation Secrets of Steve Jobs: Insanely Different Principles for Breakthrough Success"
        },
        "OfferSummary": {
          "LowestNewPrice": {
            "Amount": "1224",
            "CurrencyCode": "USD",
            "FormattedPrice": "$12.24"
          },
          "LowestUsedPrice": {
            "Amount": "1208",
            "CurrencyCode": "USD",
            "FormattedPrice": "$12.08"
          },
          "TotalNew": "49",
          "TotalUsed": "27",
          "TotalCollectible": "0",
          "TotalRefurbished": "0"
        },
        "Offers": {
          "TotalOffers": "1",
          "TotalOfferPages": "1",
          "Offer": {
            "Merchant": {
              "MerchantId": "Deprecated"
            },
            "OfferAttributes": {
              "Condition": "New"
            },
            "OfferListing": {
              "OfferListingId": "ABC123",
              "Price": {
                "Amount": "1650",
                "CurrencyCode": "USD",
                "FormattedPrice": "$16.50"
              },
              "AmountSaved": {
                "Amount": "850",
                "CurrencyCode": "USD",
                "FormattedPrice": "$8.50"
              },
              "PercentageSaved": "34",
              "Availability": "Usually ships in 24 hours",
              "AvailabilityAttributes": {
                "AvailabilityType": "now",
                "MinimumHours": "0",
                "MaximumHours": "0"
              },
              "IsEligibleForSuperSaverShipping": "1"
            }
          }
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "0071636080",
            "Title": "The Presentation Secrets of Steve Jobs: How to Be Insanely Great in Front of Any Audience"
          }, {
            "ASIN": "1591842972",
            "Title": "Inside Steve's Brain, Expanded Edition"
          }, {
            "ASIN": "1593156391",
            "Title": "The Steve Jobs Way: iLeadership for a New Generation"
          }, {
            "ASIN": "1932841660",
            "Title": "I, Steve: Steve Jobs in His Own Words"
          }, {
            "ASIN": "1451648537",
            "Title": "Steve Jobs"
          }]
        }
      }, {
        "ASIN": "B002Z8IWMS",
        "DetailPageURL": "http://",
        "ItemLinks": {
          "ItemLink": [{
            "Description": "Technical Details",
            "URL": "http://"
          }, {
            "Description": "Add To Baby Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wedding Registry",
            "URL": "http://"
          }, {
            "Description": "Add To Wishlist",
            "URL": "http://"
          }, {
            "Description": "Tell A Friend",
            "URL": "http://"
          }, {
            "Description": "All Customer Reviews",
            "URL": "http://"
          }, {
            "Description": "All Offers",
            "URL": "http://"
          }]
        },
        "SalesRank": "5046",
        "SmallImage": {
          "URL": "http://",
          "Height": {
            "#": "75",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "44",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "MediumImage": {
          "URL": "http://",
          "Height": {
            "#": "160",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "94",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "LargeImage": {
          "URL": "http://",
          "Height": {
            "#": "500",
            "@": {
              "Units": "pixels"
            }
          },
          "Width": {
            "#": "295",
            "@": {
              "Units": "pixels"
            }
          }
        },
        "ImageSets": {
          "ImageSet": {
            "@": {
              "Category": "primary"
            },
            "SwatchImage": {
              "URL": "http://",
              "Height": {
                "#": "30",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "18",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "SmallImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "44",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "ThumbnailImage": {
              "URL": "http://",
              "Height": {
                "#": "75",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "44",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "TinyImage": {
              "URL": "http://",
              "Height": {
                "#": "110",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "65",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "MediumImage": {
              "URL": "http://",
              "Height": {
                "#": "160",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "94",
                "@": {
                  "Units": "pixels"
                }
              }
            },
            "LargeImage": {
              "URL": "http://",
              "Height": {
                "#": "500",
                "@": {
                  "Units": "pixels"
                }
              },
              "Width": {
                "#": "295",
                "@": {
                  "Units": "pixels"
                }
              }
            }
          }
        },
        "ItemAttributes": {
          "Author": "Carmine Gallo",
          "Manufacturer": "McGraw-Hill",
          "ProductGroup": "eBooks",
          "Title": "The Presentation Secrets of Steve Jobs: How to Be Insanely Great in Front of Any Audience"
        },
        "SimilarProducts": {
          "SimilarProduct": [{
            "ASIN": "B004477DIW",
            "Title": "The Innovation Secrets of Steve Jobs : Insanely Different Principles for Breakthrough Success"
          }, {
            "ASIN": "B000WJSCZE",
            "Title": "Presentation Zen: Simple Ideas on Presentation Design and Delivery, ePub"
          }, {
            "ASIN": "B004GNFO7Q",
            "Title": "Apple's Cores 1: Steve Jobs and the Power of Passion"
          }, {
            "ASIN": "B005UQLLCU",
            "Title": "I, Steve: Steve Jobs in His Own Words"
          }, {
            "ASIN": "B004W2UBYW",
            "Title": "Steve Jobs"
          }]
        }
      }],
      "SearchBinSets": {
        "SearchBinSet": {
          "@": {
            "NarrowBy": "Categories"
          },
          "Bin": [{
            "BinName": "Books",
            "BinItemCount": "915",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Books"
            }
          }, {
            "BinName": "KindleStore",
            "BinItemCount": "256",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "KindleStore"
            }
          }, {
            "BinName": "Miscellaneous",
            "BinItemCount": "202",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Miscellaneous"
            }
          }, {
            "BinName": "DVD",
            "BinItemCount": "185",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "DVD"
            }
          }, {
            "BinName": "Kitchen",
            "BinItemCount": "107",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Kitchen"
            }
          }, {
            "BinName": "Electronics",
            "BinItemCount": "103",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Electronics"
            }
          }, {
            "BinName": "UnboxVideo",
            "BinItemCount": "99",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "UnboxVideo"
            }
          }, {
            "BinName": "MP3Downloads",
            "BinItemCount": "80",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "MP3Downloads"
            }
          }, {
            "BinName": "Wireless",
            "BinItemCount": "64",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Wireless"
            }
          }, {
            "BinName": "Apparel",
            "BinItemCount": "32",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Apparel"
            }
          }, {
            "BinName": "Music",
            "BinItemCount": "32",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Music"
            }
          }, {
            "BinName": "Tools",
            "BinItemCount": "18",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Tools"
            }
          }, {
            "BinName": "VHS",
            "BinItemCount": "12",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "VHS"
            }
          }, {
            "BinName": "OfficeProducts",
            "BinItemCount": "8",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "OfficeProducts"
            }
          }, {
            "BinName": "ArtsAndCrafts",
            "BinItemCount": "5",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "ArtsAndCrafts"
            }
          }, {
            "BinName": "Toys",
            "BinItemCount": "5",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Toys"
            }
          }, {
            "BinName": "GiftCards",
            "BinItemCount": "4",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "GiftCards"
            }
          }, {
            "BinName": "PetSupplies",
            "BinItemCount": "4",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "PetSupplies"
            }
          }, {
            "BinName": "MobileApps",
            "BinItemCount": "3",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "MobileApps"
            }
          }, {
            "BinName": "Automotive",
            "BinItemCount": "3",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Automotive"
            }
          }, {
            "BinName": "Software",
            "BinItemCount": "2",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Software"
            }
          }, {
            "BinName": "SportingGoods",
            "BinItemCount": "2",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "SportingGoods"
            }
          }, {
            "BinName": "Beauty",
            "BinItemCount": "1",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Beauty"
            }
          }, {
            "BinName": "HealthPersonalCare",
            "BinItemCount": "1",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "HealthPersonalCare"
            }
          }, {
            "BinName": "Jewelry",
            "BinItemCount": "1",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "Jewelry"
            }
          }, {
            "BinName": "VideoGames",
            "BinItemCount": "1",
            "BinParameter": {
              "Name": "SearchIndex",
              "Value": "VideoGames"
            }
          }]
        }
      }
    }
  };


  // Add Api to Api Directory

  function addApi(api) {
    apiDirectory[api.name] = api;
  }

  // Api object constructor

  function Api(name, displayName, description, image) {
    this.name = name;
    this.displayName = displayName;
    this.description = description;
    this.image = image;
    this.callData = {};
  }

  // Add ApiCall to Api

  function addApiCall(apiName, apiCall) {
    apiDirectory[apiName].callData[apiCall.name] = apiCall;
  }

  // ApiCall object constructor

  function ApiCall(callName, description, jsonData) {
    this.name = callName;
    this.description = description;
    this.jsonData = jsonData;
    this.fieldData = {};
  }

  // Add data to api

  function addApiCallFieldData(apiName, apiCallName, fieldKey, fieldName, dataType) {

    // Check if the API exists in the directory
    if (!apiDirectory[apiName]) {
      return;
    }

    // Create field key object if it does not exist
    if (!apiDirectory[apiName].callData[apiCallName].fieldData[fieldKey]) {
      apiDirectory[apiName].callData[apiCallName].fieldData[fieldKey] = {};
    }

    // Store values
    apiDirectory[apiName].callData[apiCallName].fieldData[fieldKey].fieldName = fieldName;
    apiDirectory[apiName].callData[apiCallName].fieldData[fieldKey].dataType = dataType;
  }

  // Transform object constructor

  function Transform(fieldKey, instanceKey, transformData) {
    this.fieldKey = fieldKey;
    this.instanceKey = instanceKey;
    this.transformData = transformData;
  }

  // Add transformation

  function addTransform(fieldKey, instanceKey, transformData) {
    transformList.push(new Transform(fieldKey, instanceKey, transformData));
  }

  // Capture mouse coordinates
  var currentMousePos = {
    x: -1,
    y: -1
  };
  $(document).mousemove(function (event) {
    currentMousePos.x = event.pageX;
    currentMousePos.y = event.pageY;
  });


  // Static sample data
  // TODO: This will be sourced from real API calls

  // Add APIs
  addApi(new Api('facebook', 'Facebook', 'Facebook API', 'http://dl.dropbox.com/u/40036711/Images/facebook-icon-32.png'));
  addApi(new Api('twitter', 'Twitter', 'Twitter API', 'http://dl.dropbox.com/u/40036711/Images/twitter-icon-32.png'));
  addApi(new Api('foursquare', 'Foursquare', 'Foursquare API', 'http://dl.dropbox.com/u/40036711/Images/foursquare-icon-32.png'));

  // Add API calls
  var fbCall1 = {
    "interaction": {
      "type": "facebook",
      "content": "Presidential Elections!",
      "source": "Facebook for iPhone",
      "id": "1e150e26da22a780e06635591f6759f4",
      "created_at": "Mon, 06 Feb 2012 16:48:43 +0000"
    },
    "facebook": {
      "likes": {
        "count": 4,

        "names": [
          "George Washington",
          "John Adams",
          "Thomas Jefferson",
          "James Madison"
        ],
        "ids": [
          "1111111111",
          "2222222222",
          "3333333333",
          "4444444444"
        ]
      },
      "application": "Facebook for iPhone",
      "type": "status",
      "message": "Presidential Elections!",
      "source": "Facebook for iPhone (6628568379)",
      "id": "570568817_10150583661558858"
    }
  };
  addApiCall('facebook', new ApiCall('FB Call 1', 'This is just a static test call.', fbCall1));

  var fbCall2 = {
    "data": [{
      "id": "X999_Y999",
      "from": {
        "name": "Tom Brady",
        "id": "X12"
      },
      "message": "Looking forward to 2010!",
      "actions": [{
        "name": "Comment",
        "link": "http://www.facebook.com/X999/posts/Y999"
      }, {
        "name": "Like",
        "link": "http://www.facebook.com/X999/posts/Y999"
      }],
      "type": "status",
      "created_time": "2010-08-02T21:27:44+0000",
      "updated_time": "2010-08-02T21:27:44+0000"
    }, {
      "id": "X998_Y998",
      "from": {
        "name": "Peyton Manning",
        "id": "X18"
      },
      "message": "Where's my contract?",
      "actions": [{
        "name": "Comment",
        "link": "http://www.facebook.com/X998/posts/Y998"
      }, {
        "name": "Like",
        "link": "http://www.facebook.com/X998/posts/Y998"
      }],
      "type": "status",
      "created_time": "2010-08-02T21:27:44+0000",
      "updated_time": "2010-08-02T21:27:44+0000"
    }]
  };
  addApiCall('facebook', new ApiCall('Example FB Call 2', 'This is another static test call.', fbCall2));

  var twitterCall1 = {
    "text": "RT @PostGradProblem: In preparation for the NFL lockout, I will be spending twice as much time analyzing my fantasy baseball team during ...",
    "truncated": true,
    "in_reply_to_user_id": null,
    "in_reply_to_status_id": null,
    "favorited": false,
    "source": "<a href=\"http://twitter.com/\" rel=\"nofollow\">Twitter for iPhone</a>",
    "in_reply_to_screen_name": null,
    "in_reply_to_status_id_str": null,
    "id_str": "54691802283900928",
    "entities": {
      "user_mentions": [{
        "indices": [
          3,
          19
        ],
        "screen_name": "PostGradProblem",
        "id_str": "271572434",
        "name": "PostGradProblems",
        "id": 271572434
      }],
      "urls": [],
      "hashtags": []
    },
    "contributors": null,
    "retweeted": false,
    "in_reply_to_user_id_str": null,
    "place": null,
    "retweet_count": 4,
    "created_at": "Sun Apr 03 23:48:36 +0000 2011",
    "retweeted_status": {
      "text": "In preparation for the NFL lockout, I will be spending twice as much time analyzing my fantasy baseball team during company time. #PGP",
      "truncated": false,
      "in_reply_to_user_id": null,
      "in_reply_to_status_id": null,
      "favorited": false,
      "source": "<a href=\"http://www.hootsuite.com\" rel=\"nofollow\">HootSuite</a>",
      "in_reply_to_screen_name": null,
      "in_reply_to_status_id_str": null,
      "id_str": "54640519019642881",
      "entities": {
        "user_mentions": [],
        "urls": [],
        "hashtags": [{
          "text": "PGP",
          "indices": [
            130,
            134
          ]
        }]
      },
      "contributors": null,
      "retweeted": false,
      "in_reply_to_user_id_str": null,
      "place": null,
      "retweet_count": 4,
      "created_at": "Sun Apr 03 20:24:49 +0000 2011",
      "user": {
        "notifications": null,
        "profile_use_background_image": true,
        "statuses_count": 31,
        "profile_background_color": "C0DEED",
        "followers_count": 3066,
        "profile_image_url": "http://a2.twimg.com/profile_images/1285770264/PGP_normal.jpg",
        "listed_count": 6,
        "profile_background_image_url": "http://a3.twimg.com/a/1301071706/images/themes/theme1/bg.png",
        "description": "",
        "screen_name": "PostGradProblem",
        "default_profile": true,
        "verified": false,
        "time_zone": null,
        "profile_text_color": "333333",
        "is_translator": false,
        "profile_sidebar_fill_color": "DDEEF6",
        "location": "",
        "id_str": "271572434",
        "default_profile_image": false,
        "profile_background_tile": false,
        "lang": "en",
        "friends_count": 21,
        "protected": false,
        "favourites_count": 0,
        "created_at": "Thu Mar 24 19:45:44 +0000 2011",
        "profile_link_color": "0084B4",
        "name": "PostGradProblems",
        "show_all_inline_media": false,
        "follow_request_sent": null,
        "geo_enabled": false,
        "profile_sidebar_border_color": "C0DEED",
        "url": null,
        "id": 271572434,
        "contributors_enabled": false,
        "following": null,
        "utc_offset": null
      },
      "id": 54640519019642880,
      "coordinates": null,
      "geo": null
    },
    "user": {
      "notifications": null,
      "profile_use_background_image": true,
      "statuses_count": 351,
      "profile_background_color": "C0DEED",
      "followers_count": 48,
      "profile_image_url": "http://a1.twimg.com/profile_images/455128973/gCsVUnofNqqyd6tdOGevROvko1_500_normal.jpg",
      "listed_count": 0,
      "profile_background_image_url": "http://a3.twimg.com/a/1300479984/images/themes/theme1/bg.png",
      "description": "watcha doin in my waters?",
      "screen_name": "OldGREG85",
      "default_profile": true,
      "verified": false,
      "time_zone": "Hawaii",
      "profile_text_color": "333333",
      "is_translator": false,
      "profile_sidebar_fill_color": "DDEEF6",
      "location": "Texas",
      "id_str": "80177619",
      "default_profile_image": false,
      "profile_background_tile": false,
      "lang": "en",
      "friends_count": 81,
      "protected": false,
      "favourites_count": 0,
      "created_at": "Tue Oct 06 01:13:17 +0000 2009",
      "profile_link_color": "0084B4",
      "name": "GG",
      "show_all_inline_media": false,
      "follow_request_sent": null,
      "geo_enabled": false,
      "profile_sidebar_border_color": "C0DEED",
      "url": null,
      "id": 80177619,
      "contributors_enabled": false,
      "following": null,
      "utc_offset": -36000
    },
    "id": 54691802283900930,
    "coordinates": null,
    "geo": null
  };
  addApiCall('twitter', new ApiCall('Example TWT Call 1', 'This is just a static test call.', twitterCall1));

  var fourSquareCall1 = {
    meta: {
      code: 200
    },
    notifications: [{
      type: "notificationTray",
      item: {
        unreadCount: 0
      }
    }],
    response: {
      checkin: {
        id: "4d627f6814963704dc28ff94",
        createdAt: 1298300776,
        type: "checkin",
        shout: "Another one of these days. #snow",
        timeZoneOffset: -300,
        user: {
          id: "32",
          firstName: "Dens",
          photo: {
            prefix: "https://irs0.4sqi.net/img/user/",
            suffix: "/32_1239135232.jpg",

          },

        },
        venue: {
          id: "408c5100f964a520c6f21ee3",
          name: "Tompkins Square Park",
          contact: {
            phone: "2123877685",
            formattedPhone: "(212) 387-7685",

          },
          location: {
            address: "E 7th St. to E 10th St.",
            crossStreet: "btwn Ave. A & B",
            lat: 40.72651075083395,
            lng: -73.98171901702881,
            postalCode: "10009",
            city: "New York",
            state: "NY",
            country: "United States",
            cc: "US",

          },
          categories: [{
              id: "4bf58dd8d48988d163941735",
              name: "Park",
              pluralName: "Parks",
              shortName: "Park",
              icon: {
                prefix: "https://foursquare.com/img/categories_v2/parks_outdoors/park_",
                suffix: ".png",

              },
              primary: true,

            },

          ],
          verified: true,
          stats: {
            checkinsCount: 25523,
            usersCount: 8932,
            tipCount: 85,

          },
          url: "http://www.nycgovparks.org/parks/tompkinssquarepark",
          likes: {
            count: 0,
            groups: [

            ],

          },
          specials: {
            count: 0,

          },

        },
        source: {
          name: "foursquare for Web",
          url: "https://foursquare.com/"
        },
        photos: {
          count: 1,
          items: [{
            id: "4d627f80d47328fd96bf3448",
            createdAt: 1298300800,
            prefix: "https://irs3.4sqi.net/img/general/",
            suffix: "/UBTEFRRMLYOHHX4RWHFTGQKSDMY14A1JLHURUTG5VUJ02KQ0.jpg",
            width: 720,
            height: 540,
            user: {
              id: "32",
              firstName: "Dens",
              photo: {
                prefix: "https://irs0.4sqi.net/img/user/",
                suffix: "/32_1239135232.jpg",

              },

            },
            visibility: "priviate"
          }],

        },
        likes: {
          count: 0,
          groups: [

          ],

        },
        like: false,
        score: {
          total: 1,
          scores: [{
              points: 1,
              icon: "https://foursquare.com/img/points/defaultpointsicon2.png",
              message: "Have fun out there!",

            },

          ],

        },

      },

    },

  };
  addApiCall('foursquare', new ApiCall('Example FSQ Call 1', 'This is just a static test call.', fourSquareCall1));

  var resultStart = {
    'start': 'node'
  };

  // isArray([]) returns true, isArray({}) returns false

  function isArray(testObject) {
    if (testObject) {
      return (testObject.constructor == Array);
    }
  }

  // Build HTML tag tree from JSON for jsTree

  function buildTagTreeFromJSON(data, parentDiv, apiName, apiCallName) {

    //debugger;
    if (!parentDiv) {
      console.error('No parent div found!');
      return;
    }

    if (!data) {
      //console.error('No data provided!');
      return;
    }

    var isRoot = false;

    if (!apiName) {
      apiName = "root";
      isRoot = true;
    }

    for (var fieldName in data) {

      // Do a string replacement for invalid tag characters
      if (fieldName === '@') {
        fieldName = 'at';
      }

      // Generate node id: <apiName>-<fieldName>-<fieldKey>
      var fieldKey = uniqueKeyGenerator();
      var id = apiName + '-' + fieldName + '-' + fieldKey;

      // Skip root node
      if (!isRoot) {
        // Update API data
        addApiCallFieldData(apiName, apiCallName, fieldKey, fieldName, typeof data[fieldName]);
      }

      // Handle arrays
      if (isArray(data[fieldName])) {
        parentDiv.append('<li id="' + id + '" rel="' + apiName + '"><a href="#">' + fieldName + ' [Array]</a><ul id="ul-' + id + '"></ul></li>');
        var newDiv = parentDiv.find('#ul-' + id);

        if (enableArrayObjectElements) {
          // Handle array elements

          // Iterate through each key of array
          for (var key in data[fieldName]) {
            if (data[fieldName].hasOwnProperty(key)) {

              if (typeof data[fieldName][key] === 'object') {
                buildTagTreeFromJSON(data[fieldName][key], newDiv, apiName, apiCallName);
              }
            }
          }
        }

        // Handle objects
      } else if (typeof data[fieldName] === 'object') {
        parentDiv.append('<li id="' + id + '" rel="' + apiName + '"><a href="#">' + fieldName + '</a><ul id="ul-' + id + '"></ul></li>');
        var newDiv = parentDiv.find('#ul-' + id);

        buildTagTreeFromJSON(data[fieldName], newDiv, apiName, apiCallName);

      } else {
        // Handle fields
        parentDiv.append('<li id="' + id + '" rel="' + apiName + '"><a href="#">' + fieldName + '</a></li>');
      }
    }
  }


  function guidGenerator() {
    var delim = 'X'
    var S4 = function () {
      /*jslint bitwise: true*/
      var retValue = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      /*jslint bitwise: false*/
      return retValue;
    };
    return (S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + S4() + S4() + S4());
  }

  function uniqueKeyGenerator() {
    var delim = 'X'
    var S4 = function () {
      /*jslint bitwise: true*/
      var retValue = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      /*jslint bitwise: false*/
      return retValue;
    };
    return (S4() + S4() + delim + S4());
  }


  function setupTypes() {
    var typeDef = {};

    // Root node
    typeDef["root"] = {
      "valid_children": "all",
      "icon": {
        "image": "components/jquery.jstree/themes/default/root.png"
      },
      // Restrict actions on start node
      "start_drag": false,
      "move_node": false,
      "delete_node": false,
      "remove": false
    };

    // The default type
    typeDef["default"] = {
      // I want this type to have no children (so only leaf nodes)
      // In my case - those are files
      "valid_children": "none",
      // If we specify an icon for the default type it WILL OVERRIDE the theme icons
      "icon": {
        "image": "components/jquery.jstree/themes/default/api/unknown.png"
      }
    };

    var definedTypes = ['facebook', 'twitter', 'foursquare'];

    for (var i = 0; i < definedTypes.length; i++) {
      typeDef[definedTypes[i]] = {
        "valid_children": "all",
        "icon": {
          "image": "components/jquery.jstree/themes/default/api/" + definedTypes[i] + ".png"
        }
      }
    }

    return typeDef;
  }


  function loadToolPane() {

    // TO CREATE AN INSTANCE
    // select the tree container using jQuery
    $('#tool-pane')
    // call `.jstree` with the options object
    .jstree({

      // the `plugins` array allows you to configure the active plugins on this instance
      'plugins': ['themes', 'html_data', 'ui', 'crrm', 'dnd', 'types', 'contextmenu'],
      'themes': {
        'theme': 'default',
        'url': 'components/jquery.jstree/themes/default/style.css'
      },

      'crrm': {
        'move': {
          'check_move': function (m) {
            return false;
          }
        }
      },

      "ui": {
        "select_multiple_modifier": "alt"
      },

      "types": {
        // I set both options to -2, as I do not need depth and children count checking
        // Those two checks may slow jstree a lot, so use only when needed
        "max_depth": -2,
        "max_children": -2,
        "type_attr": "rel",
        // I want only `start` nodes to be root nodes
        // This will prevent moving or creating any other type as a root node
        "valid_children": ["start"],

        "types": setupTypes()
      },

      'contextmenu': {
        "items": function ($node) {
          return {
            // Some key
            "action1": {
              // The item label
              "label": "Add operation",
              // The function to execute upon a click
              "action": function (obj) {

                // Get selected nodes
                var selectNodes = $.jstree._focused()._get_node(null, true);


                var nodeTagsHtml = '';
                // Iterate through nodes and build list
                selectNodes.each(function (i) {

                  var nodeIdStr = $(this).attr('id');
                  var a = nodeIdStr.indexOf('-');
                  var b = nodeIdStr.indexOf('-', a + 1);
                  var nodeName = nodeIdStr.substring(a + 1, b);

                  console.log(selectNodes[i]);

                  nodeTagsHtml = nodeTagsHtml + '<span class="label label-default">' + nodeName + '</span> ';

                  if (i == selectNodes.length - 1) {
                    nodeTagsHtml = '<div>' + nodeTagsHtml + '</div><br />';
                    $('#transform-area').append('<div id="add-operation-dialog" title="Add operation"><p>Attributes:</p>' + nodeTagsHtml + '<div class="btn-group"><a class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Add Operation <span class="caret"></span></a><ul class="dropdown-menu"><li><a href="#">Concatenate</a></li><li><a href="#">Substring</a></li></ul></div></div>');

                    // UI Dialog
                    $("#add-operation-dialog").dialog({
                      autoOpen: true,
                      position: 'center',
                      resizable: true,
                      width: 400,
                      height: 400,
                      buttons: {
                        "Accept": function () {
                          $(this).dialog("close");
                        },
                        "Cancel": function () {
                          $(this).dialog("close");
                        }
                      },
                      show: {
                        effect: "fade",
                        duration: 250
                      },
                      hide: {
                        effect: "fade",
                        duration: 250
                      }
                    });

                  }
                });



              },


              // All below are optional
              "_disabled": false, // clicking the item won't do a thing
              "_class": "class", // class is applied to the item LI node
              "separator_before": false, // Insert a separator before the item
              "separator_after": true, // Insert a separator after the item
              // false or string - if does not contain `/` - used as classname
              "icon": false //,
              //"submenu": {
              /* Collection of objects (the same structure) */
              //}
            }
          };
        }

      },

      'dnd': {
        'drop_finish': function (data) {
          return false;

        },
        'drag_check': function (data) {
          return false;
        },
        'drag_finish': function (data) {
          return false;
        }
      }
    })
    // EVENTS
    // each instance triggers its own events - to process those listen on the container
    // all events are in the `.jstree` namespace
    // so listen for `function_name`.`jstree` - you can function names from the docs

    .bind('loaded.jstree', function (event, data) {
      // you get two params - event & data - check the core docs for a detailed description
      console.log("Left tree loaded!");
    });
  }

  function loadResultPane() {
    // TO CREATE AN INSTANCE
    // select the tree container using jQuery
    $('#result-pane')
    // call `.jstree` with the options object
    .jstree({

      // the `plugins` array allows you to configure the active plugins on this instance
      'plugins': ['themes', 'html_data', 'ui', 'crrm', 'dnd', 'types', 'contextmenu'],
      'themes': {
        'theme': 'default',
        'url': 'components/jquery.jstree/themes/default/style.css'
      },

      'crrm': {
        'move': {
          'always_copy': 'multitree',
          'check_move': function (m) {
            /*
                // Moves are only allowed inside
                if(m.p=="before"|| m.p=="after")
                  return false;
                else
                  return true;*/
            return true;
          }
        }
      },


      "types": {
        // I set both options to -2, as I do not need depth and children count checking
        // Those two checks may slow jstree a lot, so use only when needed
        "max_depth": -2,
        "max_children": -2,
        "type_attr": "rel",
        // I want only `start` nodes to be root nodes
        // This will prevent moving or creating any other type as a root node
        //"valid_children": ["start"],

        "types": setupTypes()
      },

      'contextmenu': {
        "items": function ($node) {
          return {
            // Some key
            "renameField": {
              // The item label
              "label": "Rename field",
              // The function to execute upon a click
              "action": function (obj) {

                // Get selected nodes
                var selectNodes = $.jstree._focused()._get_node(null, true);


                var nodeTagsHtml = '';
                // Iterate through nodes and build list
                selectNodes.each(function (i) {

                  var nodeIdStr = $(this).attr('id');

                  // Separate ids
                  var token = nodeIdStr.split('-');
                  var apiName = token[0];
                  var captionName = token[1];
                  var fieldKey = token[2];
                  var instanceKey = token[3];

                  console.log(selectNodes[i]);

                  // Lookup the field name
                  if (!apiDirectory[apiName]) {
                    alert("Undefined API.");
                  }

                  // Retrieve field name
                  var fieldName = apiDirectory[selectedApiName].callData[selectedApiCallName].fieldData[fieldKey].fieldName;
                  nodeTagsHtml = nodeTagsHtml + '<span class="label label-default">' + fieldName + '</span> ';

                  if (i == selectNodes.length - 1) {
                    nodeTagsHtml = '<div>' + nodeTagsHtml + '</div><br />';

                    var uid = uniqueKeyGenerator();
                    var dialogId = 'operation-dialog-' + uid;
                    $('#transform-area').append('<div id="' + dialogId + '" title="Rename field"><p>Selected field:</p>' + nodeTagsHtml + '<input type="text" placeholder="New field name" id="newFieldName_' + uid + '" autocomplete="off"></div>');

                    // UI Dialog
                    $("#" + dialogId).dialog({
                      autoOpen: true,
                      position: 'center',
                      resizable: true,
                      width: 400,
                      height: 250,
                      buttons: {
                        "Accept": function () {
                          // Rename operation

                          // Retrieve dialog field
                          var newFieldName = document.getElementById('newFieldName_' + uid).value;

                          // Rename caption name
                          $.jstree._focused().rename_node($('#' + nodeIdStr), fieldName + ' -> ' + newFieldName);

                          // Add transform
                          addTransform(fieldKey, instanceKey, {
                            renameField: fieldName + ' -> ' + newFieldName
                          });

                          $(this).dialog("close");
                        },
                        "Cancel": function () {
                          $(this).dialog("close");
                        }
                      },
                      show: {
                        effect: "fade",
                        duration: 250
                      },
                      hide: {
                        effect: "fade",
                        duration: 250
                      }
                    });

                  }
                });



              },


              // All below are optional
              "_disabled": false, // clicking the item won't do a thing
              "_class": "class", // class is applied to the item LI node
              "separator_before": false, // Insert a separator before the item
              "separator_after": true, // Insert a separator after the item
              // false or string - if does not contain `/` - used as classname
              "icon": false //,
              //"submenu": {
              /* Collection of objects (the same structure) */
              //}
            }
          };
        }

      }
    })
    // EVENTS
    // each instance triggers its own events - to process those listen on the container
    // all events are in the `.jstree` namespace
    // so listen for `function_name`.`jstree` - you can function names from the docs
    .bind("move_node.jstree", function (e, data) {
      console.log('move_node.jstree detected in result.');

      // Rename starting from root of tree
      renameTagTree(data.rslt.oc);

      data.rslt.o.each(function (i) {

        // On bad moves, rollback
        if (data.inst._get_parent(data.rslt.oc) === -1) {
          $.jstree.rollback(data.rlbk);
        } else {

          // Create shape in workflow area
          var headerHeight = $('#content-header').height();
          var tpWidth = $('#tool-pane').width();

          var nodeWidth = 140;
          var nodeHeight = 32;

          //var shapeX = (currentMousePos.x - tpWidth) - (nodeWidth / 2);
          //var shapeY = (currentMousePos.y - headerHeight) + (nodeHeight / 2);

          var prevShapeX = 0;
          var shapeX = tpWidth + 32 + prevShapeX;
          var shapeY = (currentMousePos.y - headerHeight) + (nodeHeight / 2);

          if (shapeX < tpWidth) {
            shapeX = tpWidth;
          }


          var nodeIdStr = $(this).attr('id');
          var a = nodeIdStr.indexOf('-');
          var b = nodeIdStr.indexOf('-', a + 1);
          var nodeName = nodeIdStr.substring(a + 1, b);

          createShape('Test', nodeName, [shapeX, shapeY], 0, $('#scrollable-canvas'));


          /*
          // Check if this is a parent
          if (data.rslt.cy && $(data.rslt.oc).children("UL").length) {
            $(data.rslt.oc).attr("rel", "parent");
          } else {
            $(data.rslt.oc).attr("rel", "leaf");
          }
          */
        }
      });
    })


    .bind('loaded.jstree', function (event, data) {
      // you get two params - event & data - check the core docs for a detailed description
      console.log("Right tree loaded!");

    });
  }

  /*var overlaps = (function () {
    function getPositions(elem) {
      var pos, width, height;
      pos = $(elem).position();
      width = $(elem).width();
      height = $(elem).height();
      return [[pos.left, pos.left + width], [pos.top, pos.top + height]];
    }

    function comparePositions(p1, p2) {
      var r1, r2;
      r1 = p1[0] < p2[0] ? p1 : p2;
      r2 = p1[0] < p2[0] ? p2 : p1;
      return r1[1] > r2[0] || r1[0] === r2[0];
    }

    return function (a, b) {
      var pos1 = getPositions(a),
        pos2 = getPositions(b);
      return comparePositions(pos1[0], pos2[0]) && comparePositions(pos1[1], pos2[1]);
    };
  })();*/
  /*
  function buildShapesFromJSON(data, parentKey, parentDiv, parentCoords) {
    if (!parentDiv) {
      console.error('No parent div found!');
      return;
    }

    var i, j;
    var xCoord = parentCoords[0];
    var yCoord = parentCoords[1];


    var objCount = 0;
    var attrCount = 0;
    for (i in data) {

      var guid = 'data-elem-' + i + '-' + guidGenerator();



      if (!parentKey) {
        parentChildMap.root = [guid];
      } else {
        if (!parentChildMap[parentKey]) {
          parentChildMap[parentKey] = [];
        }
        parentChildMap[parentKey].push(guid);
      }

      console.log('key: ' + parentKey + ', value = ' + guid);

      var shapeData;

      if (typeof data[i] === 'object') {
        // Reset coordinates        
        xCoord = parentCoords[0] + (220 * objCount);
        yCoord = parentCoords[1] + (50 * attrCount);

        shapeData = '<div id="' + guid + '" class="shape parent" data-shape="Rectangle" style="left: ' + xCoord + 'px; top: ' + yCoord + 'px;">' + i + '</div>';
        parentDiv.append(shapeData);
        buildShapesFromJSON(data[i], guid, parentDiv, [xCoord + 20, yCoord + 50]);
        // Increment objects
        objCount++;
      } else {
        shapeData = '<div id="' + guid + '" class="shape leaf" data-shape="Rectangle" style="left: ' + xCoord + 'px; top: ' + yCoord + 'px;"><img src="../../img/icon_facebook.png" width="24" height="24" class="icon" />' + i + '</div>';
        parentDiv.append(shapeData);
        xCoord = xCoord + 160;
        // Increment attributes
        attrCount++;
      }
    }

    console.log('Data element overlap check:');
    var shapes = $('.shape');

    console.log('Number of data divs = ' + shapes.length);
    for (i = 0; i < shapes.length; i++) {
      for (j = i + 1; j < shapes.length; j++) {
        console.log('shapes[' + i + '] vs. shapes[' + j + '] = ' + overlaps(shapes[i], shapes[j]));
      }
    }


    if (!parentKey) {
      console.log(parentChildMap);
    }
  }*/

  function renameTagTree(parentData) {

    // Handle multiple selections
    if (parentData.length > 1) {
      for (var i = 0; i < parentData.length; i++) {
        // Recurse each tag
        renameTagTree(parentData[i]);
      }
      return;
    }

    // Rename parent
    if ($(parentData).is('li')) {
      // Remove 'copy_' prefix and append instance key
      $(parentData).attr("id", $(parentData).attr('id').substring('copy_'.length) + "-" + uniqueKeyGenerator());
    }

    var ulItems = $(parentData).children('ul');
    var liItems = $(parentData).children('li');

    for (var i = 0; i < ulItems.length; i++) {
      var ulItem = $(ulItems[i]);

      // Remove 'copy_ul' prefix and append instance key
      $(ulItem).attr("id", 'ul-' + $(ulItem).attr('id').substring('copy_ul-'.length) + "-" + uniqueKeyGenerator());

      // Rename rest of lists
      renameTagTree(ulItem);
    }

    for (var j = 0; j < liItems.length; j++) {
      var liItem = $(liItems[j]);

      // Rename item
      renameTagTree(liItem);
    }
  }

  function createShape(id, caption, pos, type, parentDiv) {
    var xCoord = pos[0];
    var yCoord = pos[1];

    var guid = 'data-elem-' + id + '-' + guidGenerator();

    var shapeData;
    if (type === 0) {
      shapeData = '<div id="' + guid + '" class="shape parent" data-shape="Rectangle" style="postion: absolute; left: ' + xCoord + 'px; top: ' + yCoord + 'px;">' + caption + '</div>';
    } else {
      shapeData = '<div id="' + guid + '" class="shape leaf" data-shape="Rectangle" style="postion: absolute; left: ' + xCoord + 'px; top: ' + yCoord + 'px;"><img src="../../img/icon_facebook.png" width="24" height="24" class="icon" />' + caption + '</div>';
    }

    // Add to parent div
    parentDiv.append(shapeData);

    // Setup jsPlumb
    jsPlumb.importDefaults({
      Connector: 'StateMachine',
      PaintStyle: {
        lineWidth: 2,
        strokeStyle: '#464646'
      },
      Endpoint: ['Dot', {
        radius: 3
      }],
      EndpointStyle: {
        fillStyle: '#464646'
      }
    });

    var shapeDiv = $('#' + guid);

    // make everything draggable
    jsPlumb.draggable(shapeDiv);
  }

  return Backbone.Marionette.Layout.extend({
    template: transformTpl,

    onShow: function () {

      $('#transform-area').append('<button id="transformButton">Transform Data</button><div id="selectPanel" style="width: 100%; text-align: left;"><div id="div-api-select" style="width: 250px; display: inline-block;"><select id="api-select" style="width: 250px;"></select></div><div id="div-api-call-select" style="display: inline-block;"></div></div><div id="h-split-panel" style="height: 100%;"><div id="v-split-panel"><div id="tool-pane"><ul id="ul-tool-pane"></ul></div><div id="result-pane"><ul id="ul-result-pane"></ul></div></div><div id="scrollable-canvas" class="jstree-drop"></div></div>');


      $("#transformButton").click(function () {

        // Pass sample template and data to transform API
        $.ajax({
          type: "POST",
          url: "/api/transform/apply",
          // The key needs to match your method's input parameter (case-sensitive).
          data: JSON.stringify({
            tmpl: sampleTmpl,
            data: sampleData
          }),
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          complete: function (responseData) {
            //alert(JSON.stringify(data));

            // Show reponse JSON
            debugger;

            var responseJSONData = responseData.responseJSON;

            $('#transform-area').append('<div id="transform-result-dialog" title="Transformed JSON Data">' + JSON.stringify(responseJSONData.after) + '</div>');

            // UI Dialog
            $("#transform-result-dialog").dialog({
              autoOpen: true,
              position: 'center',
              resizable: true,
              width: 400,
              height: 400,
              buttons: {
                "Accept": function () {
                  $(this).dialog("close");
                },
                "Cancel": function () {
                  $(this).dialog("close");
                }
              },
              show: {
                effect: "fade",
                duration: 250
              },
              hide: {
                effect: "fade",
                duration: 250
              }
            });

          }
        });

      });

      // Api select drop down
      var apiSelectDropDown = $("#api-select").msDropdown().data("dd");

      // Initial value
      apiSelectDropDown.add({
        text: "API",
        value: -1,
        description: 'Please choose API',
        className: ''
      });

      // Add Api options
      for (var apiName in apiDirectory) {
        apiSelectDropDown.add({
          text: apiDirectory[apiName].displayName,
          value: apiDirectory[apiName].name,
          description: apiDirectory[apiName].description,
          image: apiDirectory[apiName].image,
        });
      }

      apiSelectDropDown.set("selectedIndex", 0);
      apiSelectDropDown.on("change", function (res) {
        selectedApiName = res.target.value;

        $('#div-api-call-select').empty();
        $('#div-api-call-select').append('<select style="width:300px" id="api-call-select"></select>');

        // Api call select drop down
        var apiCallSelectDropDown = $("#api-call-select").msDropdown().data("dd");

        // Initial value
        apiCallSelectDropDown.add({
          text: "API call",
          value: -1,
          description: 'Please choose API call',
          className: ''
        });

        // Add ApiCall options
        for (var callName in apiDirectory[selectedApiName].callData) {
          apiCallSelectDropDown.add({
            text: apiDirectory[selectedApiName].callData[callName].name,
            value: apiDirectory[selectedApiName].callData[callName].name,
            description: apiDirectory[selectedApiName].callData[callName].description
          });
        }


        apiCallSelectDropDown.set("selectedIndex", 0);
        apiCallSelectDropDown.on("change", function (res) {
          selectedApiCallName = res.target.value;

          $('#tool-pane').empty();
          $('#tool-pane').append('<ul id="ul-tool-pane"></ul>');
          buildTagTreeFromJSON(apiDirectory[selectedApiName].callData[selectedApiCallName].jsonData, $('#ul-tool-pane'), selectedApiName, selectedApiCallName);
          loadToolPane();
        });
      });


      /*
      $('#api-select').ddslick({
        data: ddApiData,
        defaultSelectedIndex: 0,
        width: 300,
        imagePosition: "left",
        selectText: "Select API",
        onSelected: function (data) {
          $('#tool-pane').empty();
          $('#tool-pane').append('<ul id="ul-tool-pane"></ul>')
          selectedApi = data.selectedData.text;
          loadToolPane();

          //$('#api-call-select').empty();
          //$('#api-call-select').removeClass('dd-container');

          var ddApiCallData = [];

          for (var i = 0; i < 100; i++) {
          ddApiCallData.push({
            text: 'Call ' + i + ': '+ uniqueKeyGenerator(),
            value: i,
          });
        }


        if ($('#api-call-select').children().length == 0) {
          $('#api-call-select').ddslick({
            data: ddApiCallData,
            selectText: "Select API call name",
            onSelected: function (data) {
              selectedApiCall = data.selectedData.text;
              buildTagTreeFromJSON(apiDirectory[selectedApi].jsonData, $('#ul-tool-pane'), selectedApi, selectedApiCallName);
            }
          });      
        }*/



      //}
      //});

      buildTagTreeFromJSON(resultStart, $('#ul-result-pane'), null, null);
      loadResultPane();

      //var selData = $('#api-select').data('ddslick');

      // Build shapes from JSON
      //buildShapesFromJSON(jsonData, null, $("#scrollable-canvas"), [10, 10]);
      //buildTreeFromJSON(jsonData[selData.selectedData.text], $('#ul-tool-pane'));


      // Initialize jsPlumb
      //jsPlumbDemo.init(parentChildMap);
      jsPlumb.bind('ready', function () {
        jsPlumb.setRenderMode(jsPlumb.SVG);
      });

      $('#h-split-panel').splitter({
        type: 'h',
        outline: true,
        dock: 'top',
        dockSpeed: 200,
        dockKey: 'Z', // Alt-Shift-Z in FF/IE
        accessKey: 'I' // Alt-Shift-I in FF/IE
      });

      $('#v-split-panel').splitter({
        type: 'v',
        outline: true,
        sizeLeft: 250,
        minLeft: 100,
        minRight: 100,
        resizeToWidth: true,
        dock: 'left',
        dockSpeed: 200,
        cookie: 'docksplitter',
        dockKey: 'Z', // Alt-Shift-Z in FF/IE
        accessKey: 'I' // Alt-Shift-I in FF/IE
      });

    }

  });

});