/*
 * client/js/views/home/content.js
 */

/* global define */

define([
  'jquery',
  'backbone',
  'hbs!templates/home/content'
], function ($, Backbone, contentTpl) {
  'use strict';


  return Backbone.Marionette.CompositeView.extend({
    template: contentTpl,

    serializeData: function () {
      var data = {};

      if (this.model) {
        data = this.model.toJSON();
      } else if (this.collection) {
        data = {
          items: this.collection.toJSON()
        };
      }

      if ($.cookie('user.name.full')) {
        data.user = {
          name: {
            full: $.cookie('user.name.full')
          }
        };
      }
      return data;
    }
  });
});
