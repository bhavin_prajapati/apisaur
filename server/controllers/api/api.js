/*
 * server/controllers/api/top.js
 */

'use strict';

//var fs = require('fs'),
//    path = require('path');

var _ = require('lodash'),
    app = require('../../app'),
    raml = require('raml-parser');

function _getContent(cb) {
  app.models.API.find(
    null,
    function (err, items) {
      if (items) {
        _.each(items, function (item, i) {
          item.index = i;
        });
        return cb(null, items);
      }
    });
}

function LIST(req, cb) {
  _getContent(cb);
}

function GET(req, id, cb) {
  var item = null;
  _getContent(function (err, items) {
    if (err) { return cb(err); }
    if (_.isArray(items) && items.length) {
      if (_.isNumber(items[0].id)) {
        item = _.find(items, {'id': +id});
      } else {
        item = _.find(items, {'id': id});
      }
    }
    if (item) {
      cb(null, item);
    } else {
      cb('invalid id - ' + id);
    }
  });
}

function POST(req, cb) {
  var body = '';
  req.on('data', function (data) {
    body += data;
  });
  req.on('end', function () {
    console.log(body);
    raml.load(body).then( function(apiJson) {
      console.log(apiJson);
      // Store
      app.models.API.findOne({
        'title': apiJson.title
      }, function (err, api) {
        if (err) { return cb(err); }
        if (api) {
          req.flash('error', 'API already exists.');
          return cb('API already exists - ' + req.method);
        }
        new app.models.API({
          'title' : apiJson.title,
          'raml' : JSON.stringify(apiJson)
        }).save(function (err, api) {
          if (err) { return cb(err); }
          else { return cb(null, 'API saved' + api); }
        });
      });
    }, function(error) {
      console.log('Error parsing: ' + error);
      cb(error);
    });
  });
}

function PUT(req, id, cb) {
  cb('invalid method - ' + req.method);
}

function DELETE(req, id, cb) {
  cb('invalid method - ' + req.method);
}

// Public API
exports.__filename = __filename;
exports.LIST = LIST;
exports.GET = GET;
exports.POST = POST;
exports.PUT = PUT;
exports.DELETE = DELETE;
