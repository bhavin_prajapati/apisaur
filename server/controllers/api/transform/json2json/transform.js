'use strict';

// Transform JSON object

function applyTemplate(req, res) {

	var ObjectTemplate, TemplateConfig, sysmo;

	sysmo = (typeof require === "function" ? require('./sysmo') : void 0) || (typeof window !== "undefined" && window !== null ? window.Sysmo : void 0);
	TemplateConfig = (typeof require === "function" ? require('./TemplateConfig') : void 0) || (typeof window !== "undefined" && window !== null ? window.json2json.TemplateConfig : void 0);
	ObjectTemplate = (typeof require === "function" ? require('./ObjectTemplate') : void 0) || (typeof window !== "undefined" && window !== null ? window.json2json.ObjectTemplate : void 0);

	var tmpl, data;

	if (!req.body.tmpl) {		
		res.send("Error: no transform template.");
		return;
	} else {
		tmpl = req.body.tmpl;
	}

	if (!req.body.data) {
		res.send("Error: no JSON data.");
		return;
	} else {
		data = req.body.data;
	}

	var transformObj = new ObjectTemplate(tmpl).transform(data);
	var responseData = {
		before: data,
		after: transformObj
	}
	res.send(responseData);
}

// Public API
exports.applyTemplate = applyTemplate;