/*
 * server/controllers/api/top.js
 */

'use strict';

//var fs = require('fs'),
//    path = require('path');
var http = require('http');
var xml2js = require('xml2js');
var xslt = require('node_xslt');
var async = require('async');

var parser = new xml2js.Parser();

function LIST(req, cb) {
  var url = req.param('url');
  var body = '';
  http.get(url, function(resp){
    resp.setEncoding('utf8');
    resp.on('data', function(chunk){
      body += chunk;
    });
    resp.on('end', function(){
      parser.on('end', function(result) {
        cb(null, JSON.stringify(result), {'Content-Type': 'application/json'});
      });
      parser.parseString(body,{trim: true});
      parser.reset();
    });
  }).on('error', function(e){
    console.log('Got error: ' + e.message);
  });
}

function GET(req, id, cb) {
  var xmlUrl = req.param('url');
  var xsltUrl = req.param('xslt');

  var xsltBody = '';
  var xmlBody = '';

  async.series([
    function(){
      if(xsltUrl !== null){
        http.get(xsltUrl, function(resp){
          resp.setEncoding('utf8');
          resp.on('data', function(chunk){
            xsltBody += chunk;
          });
        }).on('error', function(e){
          console.log('Got error getting xslt: ' + e.message);
        });
      }
    },
    function(){
      http.get(xmlUrl, function(resp){
        resp.setEncoding('utf8');
        resp.on('data', function(chunk){
          xmlBody += chunk;
        });
        resp.on('end', function(){
          parser.on('end', function(result) {
            cb(null, JSON.stringify(result), {'Content-Type': 'application/json'});
          });

          var xsltStylesheet = xslt.readXsltString(xsltBody);
          var xmlDocument = xslt.readXmlString(xmlBody);
          var transformedXml = xslt.transform(xsltStylesheet, xmlDocument, []);

          parser.parseString(transformedXml,{trim: true});
          parser.reset();
        });
      }).on('error', function(e){
        console.log('Got error getting: ' + e.message);
      });
    }
  ]);
}

function POST(request, cb) {
  var body = '';
  request.setEncoding('utf8');
  request.on('data', function (data) {
    body += data;
  });
  request.on('end', function () {
    parser.on('end', function(result) {
      cb(null, JSON.stringify(result), {'Content-Type': 'application/json'});
    });
    parser.parseString(body,{trim: true});
    parser.reset();
  });
}

function PUT(req, id, cb) {
  cb('invalid method - ' + req.method);
}

function DELETE(req, id, cb) {
  cb('invalid method - ' + req.method);
}

// Public API
exports.__filename = __filename;
exports.LIST = LIST;
exports.GET = GET;
exports.POST = POST;
exports.PUT = PUT;
exports.DELETE = DELETE;
