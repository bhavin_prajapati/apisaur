/*
 * server/controllers/dashboard.js
 */

'use strict';

function dashboard(req, res) {
  res.render('dashboard/dashboard');
}

function manage(req, res) {
  res.render('dashboard/manage');
}

function objects(req, res) {
  res.render('dashboard/objects');
}

function transform(req, res) {
  res.render('dashboard/transform');
}

function serve(req, res) {
  res.render('dashboard/serve');
}

function test(req, res) {
  res.render('dashboard/test');
}

function billingOverview(req, res) {
  res.render('dashboard/billing_overview');
}

function billingInvoice(req, res) {
  res.render('dashboard/billing_invoice');
}

function support(req, res) {
  res.render('dashboard/support');
}

// Public API
exports.dashboard = dashboard;
exports.manage = manage;
exports.objects = objects;
exports.transform = transform;
exports.serve = serve;
exports.test = test;
exports.billingOverview = billingOverview;
exports.billingInvoice = billingInvoice;
exports.support = support;
