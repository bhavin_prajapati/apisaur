/*
 * server/controllers/home.js
 */

'use strict';

function about(req, res) {
  res.render('home/about', {
    layout: 'layout'
  });
}

function advertise(req, res) {
  res.render('home/advertise', {
    layout: 'layout'
  });
}

function blog(req, res) {
  res.render('home/blog', {
    layout: 'layout'
  });
}

function careers(req, res) {
  res.render('home/careers', {
    layout: 'layout'
  });
}

function contact(req, res) {
  res.render('home/contact', {
    layout: 'layout'
  });
}

function express(req, res) {
  res.render('home/express', {
    layout: 'layout'
  });
}

function help(req, res) {
  res.render('home/help', {
    layout: 'layout'
  });
}

function submit(req, res) {
  res.render('home/submit', {
    layout: 'layout'
  });
}

// Public API
exports.about = about;
exports.advertise = advertise;
exports.blog = blog;
exports.careers = careers;
exports.contact = contact;
exports.express = express;
exports.help = help;
exports.submit = submit;
