/*
 * server/models/API.js
 */

'use strict';

var mongoose = require('mongoose');
var plugin = mongoose.customPlugin;


// Schema
var schema = new mongoose.Schema({
  title: { type: String, required: true },
  raml: { type: String, required: true }
});

// Plugins
schema.plugin(plugin.findOrCreate);

// Model
var model = mongoose.model('API', schema);

// Public API
exports = module.exports = model;
