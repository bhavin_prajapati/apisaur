/*
 * server/routes.js
 */

'use strict';

var ultimate = require('ultimate');

exports.register = function (app) {
  var csrf = ultimate.server.controller.csrf,
      ensureAdmin = ultimate.server.controller.ensureAdmin,
      ensureGuest = ultimate.server.controller.ensureGuest,
      ensureUser = ultimate.server.controller.ensureUser;

  var error404 = ultimate.server.route.error404,
      restify = ultimate.server.route.restify;

  var c = app.controllers,
      s = app.servers.express.getServer();

  // Pages
  s.get('/about', c.home.about);
  s.get('/advertise', c.home.advertise);
  s.get('/blog', c.home.blog);
  s.get('/careers', c.home.careers);
  s.get('/contact', c.home.contact);
  s.get('/express', c.home.express);
  s.get('/help', c.home.help);
  s.get('/submit', ensureUser, c.home.submit);

  //Dashboard
  s.get('/dashboard', ensureUser, c.dashboard.dashboard);
  s.get('/manage', ensureUser, c.dashboard.manage);
  s.get('/objects', ensureUser, c.dashboard.objects);
  s.get('/transform', ensureUser, c.dashboard.transform);
  s.get('/serve', ensureUser, c.dashboard.serve);
  s.get('/test', ensureUser, c.dashboard.test);
  s.get('/billing_overview', ensureUser, c.dashboard.billingOverview);
  s.get('/billing_invoice', ensureUser, c.dashboard.billingInvoice);
  s.get('/support', ensureUser, c.dashboard.support);

  // Account
  s.get('/account', ensureUser, c.account.index);

  // Admin
  s.get('/admin', ensureAdmin, c.admin.index);

  // API
  restify.any(s, '/api/api', c.api.api, ['list', 'get']);
  restify.any(s, '/api/api', c.api.api, ['list', 'post']);
  restify.any(s, '/api/xmltojson', c.api.xmltojson, ['list', 'get']);
  restify.any(s, '/api/xmltojson', c.api.xmltojson, ['list', 'post']);
  restify.any(s, '/api/jsontoxml', c.api.jsontoxml, ['list', 'get']);
  restify.any(s, '/api/jsontoxml', c.api.jsontoxml, ['list', 'post']);

  // Transform API
  s.post('/api/transform/apply', ensureUser, c.api.transform.json2json.transform.applyTemplate);

  // Auth
  s.get('/login', ensureGuest, c.auth.login);
  s.post('/login', ensureGuest, csrf, c.auth.loginPOST);
  s.get('/logout', c.auth.logout);
  s.post('/logout', c.auth.logoutPOST);
  s.get('/lost-password', ensureGuest, c.auth.lostPassword);
  s.post('/lost-password', ensureGuest, csrf, c.auth.lostPasswordPOST);
  s.get('/register', ensureGuest, c.auth.register);
  s.post('/register', ensureGuest, csrf, c.auth.registerPOST);
  s.get('/auth/facebook', c.auth.facebook);
  s.get('/auth/facebook/callback', c.auth.facebookCallback);
  s.get('/auth/facebook/success', c.auth.facebookSuccess);
  s.get('/auth/google', c.auth.google);
  s.get('/auth/google/callback', c.auth.googleCallback);
  s.get('/auth/google/success', c.auth.googleSuccess);
  s.get('/auth/twitter', c.auth.twitter);
  s.get('/auth/twitter/callback', c.auth.twitterCallback);
  s.get('/auth/twitter/success', c.auth.twitterSuccess);

  // Extra routes
  error404.register(s);
};